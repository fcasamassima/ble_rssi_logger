/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ucla.bluetooth.le;

import android.app.Activity;
import android.app.ListActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Region;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.ParcelUuid;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.UUID;

import com.ucla.bluetooth.le.R;

/**
 * Activity for scanning and displaying available Bluetooth LE devices.
 */
public class DeviceScanActivity extends ListActivity {
    private LeDeviceListAdapter mLeDeviceListAdapter;
    private BluetoothAdapter mBluetoothAdapter;
    private boolean mScanning;
    private Handler mHandler;

    private static final int REQUEST_ENABLE_BT = 1;
    // Stops scanning after 10 seconds.
    private static long SCAN_PERIOD = 60000, DataReceivedTime = 0;
    private SeekBar scanTimeControl = null;
    private TextView scanText = null;
    private final int  seekMax = 120;
    private final String dirName = "BLE_RSSI/";
    private static String logFolderPath = "";
    private File log_file;
    private FileWriter log_file_wr;
    private String file_name_log;
    private static Calendar rightNow;
    private BufferedWriter out_log;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.device_scan_layout);
        getActionBar().setTitle(R.string.title_devices);
        scanTimeControl = (SeekBar) findViewById(R.id.scanTimeBar);
        scanTimeControl.setMax(seekMax -1);
        scanTimeControl.setProgress((int) (SCAN_PERIOD/1000));
        scanText = (TextView) findViewById(R.id.scanTimeText);
        scanText.setText(Integer.toString((int) (SCAN_PERIOD/1000)));
        
        
        mHandler = new Handler();

        // Use this check to determine whether BLE is supported on the device.  Then you can
        // selectively disable BLE-related features.
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }

        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        
        scanTimeControl.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			int progressChanged = 0;
 
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
				progressChanged = progress +1;
			}
 
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
 
			public void onStopTrackingTouch(SeekBar seekBar) {
				progressChanged = Math.round(progressChanged/10);
				progressChanged *= 10;
				Toast.makeText(DeviceScanActivity.this,"Scan duration set to:"+progressChanged + " sec.", 
						Toast.LENGTH_SHORT).show();
				SCAN_PERIOD = progressChanged * 1000;
				scanText.setText(Integer.toString((int) (SCAN_PERIOD/1000)));
			}
		});
        createDirIfNotExists(dirName);
       // createLogFile();
        
    }
    
    private void createLogFile(){
    	
    	EditText txtDescription = (EditText) findViewById(R.id.editText1);
    	String string = txtDescription.getText().toString(); 
    	
    	file_name_log = "log_"+rightNow.get(Calendar.HOUR_OF_DAY)+"."+rightNow.get(Calendar.MINUTE)+"."+rightNow.get(Calendar.SECOND)+"_"+string+".txt";	
		
		//log_file = new File(root,"log_data/"+file_name_log);
		log_file = new File(logFolderPath,file_name_log);

		try {
			log_file_wr = new FileWriter(log_file);
			out_log = new BufferedWriter(log_file_wr);
			
			//Write File header
			out_log.write("Timestamp; RSSI; Device UUID; Device Name; \n");
			out_log.flush();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
    }
    
    public static boolean createDirIfNotExists(String path) {
        boolean ret = true;
        File root = Environment.getExternalStorageDirectory();
        TimeZone tz = TimeZone.getTimeZone("Europe/Rome");
    	rightNow = Calendar.getInstance(tz);// .getInstance();
    	String folderName = root.getAbsolutePath()+	"/"+path +rightNow.get(Calendar.DAY_OF_MONTH)+"_"+ (rightNow.get(Calendar.MONTH) + 1) +"_"+ rightNow.get(Calendar.YEAR) +"/";
    	logFolderPath = folderName;
    	//String folderName = root.getAbsolutePath()+	"/CUPID_data/"+rightNow.get(Calendar.DAY_OF_MONTH)+"_"+ (rightNow.get(Calendar.MONTH) + 1) +"_"+ rightNow.get(Calendar.YEAR) +"/";
      //  File file = new File(root.getAbsolutePath()+"/"+ path );
    	File file = new File(folderName);
        if (!file.exists()) {
            if (!file.mkdirs()) {
                Log.e("BLE RSSI", "Problem creating Image folder");
                ret = false;
            }
        }
        return ret;
    }
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        if (!mScanning) {
            menu.findItem(R.id.menu_stop).setVisible(false);
            menu.findItem(R.id.menu_scan).setVisible(true);
            menu.findItem(R.id.menu_refresh).setActionView(null);
        } else {
            menu.findItem(R.id.menu_stop).setVisible(true);
            menu.findItem(R.id.menu_scan).setVisible(false);
            menu.findItem(R.id.menu_refresh).setActionView(
                    R.layout.actionbar_indeterminate_progress);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_scan:
                mLeDeviceListAdapter.clear();
                scanLeDevice(true);
                createLogFile();
                break;
            case R.id.menu_stop:
                scanLeDevice(false);
                mLeDeviceListAdapter.clear();
                break;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
        // fire an intent to display a dialog asking the user to grant permission to enable it.
        if (!mBluetoothAdapter.isEnabled()) {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }

        // Initializes list view adapter.
        mLeDeviceListAdapter = new LeDeviceListAdapter();
        setListAdapter(mLeDeviceListAdapter);
        //scanLeDevice(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // User chose not to enable Bluetooth.
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            finish();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();
        scanLeDevice(false);
        mLeDeviceListAdapter.clear();
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        final BluetoothDevice device = mLeDeviceListAdapter.getDevice(position);
        if (device == null) return;
        final Intent intent = new Intent(this, DeviceControlActivity.class);
        intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_NAME, device.getName());
        intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_ADDRESS, device.getAddress());
        if (mScanning) {
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
            mScanning = false;
        }
        startActivity(intent);
    }

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    scanTimeControl.setEnabled(true);
                    mLeDeviceListAdapter.clear();
                    invalidateOptionsMenu();
                }
            }, SCAN_PERIOD);
            mScanning = true;
            mBluetoothAdapter.startLeScan(mLeScanCallback);
            scanTimeControl.setEnabled(false);
        } else {
            mScanning = false;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
            
        }
        invalidateOptionsMenu();
//        scanTimeControl.setEnabled(true);
    }

    // Adapter for holding devices found through scanning.
    private class LeDeviceListAdapter extends BaseAdapter {
        private ArrayList<BluetoothDevice> mLeDevices;
        private ArrayList<String> mScanRecord;
        private ArrayList<Integer> mRSSI;
        private LayoutInflater mInflator;

        public LeDeviceListAdapter() {
            super();
            mLeDevices = new ArrayList<BluetoothDevice>();
            mScanRecord = new ArrayList<String>();
            mRSSI = new ArrayList<Integer>();
            mInflator = DeviceScanActivity.this.getLayoutInflater();
        }
        
        public void addDevice(BluetoothDevice device, byte[] scanRec, int lRSSI) {
        	String objScanRec = bytesToHex(scanRec);
        	String outStr = null;
        	if (!mScanRecord.contains(objScanRec)){
        		mScanRecord.add(objScanRec);
        		mLeDevices.add(device);
        		mRSSI.add(lRSSI);
        	}
        	else {
        		int itemInd;
        		itemInd = mScanRecord.lastIndexOf(objScanRec);
        		mRSSI.set(itemInd, lRSSI);
        		DataReceivedTime = android.os.SystemClock.elapsedRealtime();
        		outStr = DataReceivedTime + " ;"+lRSSI+";"+objScanRec+";"+device.getName()+";\n";
        		try {
					out_log.write(outStr);
					out_log.flush();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        		
        	}
        	
        }
        
        
        private byte[] toByteArray (Object obj)
        {
          byte[] bytes = null;
          ByteArrayOutputStream bos = new ByteArrayOutputStream();
          try {
            ObjectOutputStream oos = new ObjectOutputStream(bos); 
            oos.writeObject(obj);
            oos.flush(); 
            oos.close(); 
            bos.close();
            bytes = bos.toByteArray ();
          }
          catch (IOException ex) {
            //TODO: Handle the exception
          }
          return bytes;
        }
            
        private Object toObject (byte[] bytes)
        {
          Object obj = null;
          try {
            ByteArrayInputStream bis = new ByteArrayInputStream (bytes);
            ObjectInputStream ois = new ObjectInputStream (bis);
            obj = ois.readObject();
          }
          catch (IOException ex) {
            //TODO: Handle the exception
          }
          catch (ClassNotFoundException ex) {
            //TODO: Handle the exception
          }
          return obj;
        }
        final protected char[] hexArray = "0123456789ABCDEF".toCharArray();      
        public String bytesToHex(byte[] bytes) {
            char[] hexChars = new char[bytes.length * 2];
            for ( int j = 0; j < bytes.length; j++ ) {
                int v = bytes[j] & 0xFF;
                hexChars[j * 2] = (char) hexArray[v >>> 4];
                hexChars[j * 2 + 1] = (char) hexArray[v & 0x0F];
                if (j==30) break;
            }
            return new String(hexChars);
        }
        
        
//        public void addDevice(BluetoothDevice device) {
//            if(!mLeDevices.contains(device)) {
//                mLeDevices.add(device);
//            }
//        }
        

        public BluetoothDevice getDevice(int position) {
            return mLeDevices.get(position);
        }

        public void clear() {
            mLeDevices.clear();
        }

        @Override
        public int getCount() {
            return mLeDevices.size();
        }

        @Override
        public Object getItem(int i) {
            return mLeDevices.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder;
            // General ListView optimization code.
            if (view == null) {
                view = mInflator.inflate(R.layout.listitem_device, null);
                viewHolder = new ViewHolder();
                viewHolder.deviceAddress = (TextView) view.findViewById(R.id.device_address);
                viewHolder.deviceName = (TextView) view.findViewById(R.id.device_name);
                viewHolder.deviceRSSI = (TextView) view.findViewById(R.id.device_RSSI);
                viewHolder.deviceUUID = (TextView) view.findViewById(R.id.device_UUID);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }

            BluetoothDevice device = mLeDevices.get(i);
            int devRSSI = mRSSI.get(i);
            String UUID = mScanRecord.get(i);
            final String deviceName = device.getName();
            if (deviceName != null && deviceName.length() > 0)
                viewHolder.deviceName.setText(deviceName);
            else
                viewHolder.deviceName.setText(R.string.unknown_device);
            	viewHolder.deviceAddress.setText(device.getAddress());
            	viewHolder.deviceRSSI.setText("RSSI:  " + devRSSI);
            	viewHolder.deviceUUID.setText("UUID:  " + UUID);

            return view;
        }
    }

    // Device scan callback.
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {

        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord) {
        	int test = rssi;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
//                	int prova = ;
                    mLeDeviceListAdapter.addDevice(device,scanRecord,rssi);
                    mLeDeviceListAdapter.notifyDataSetChanged();
                }
            });
        }
    };

    static class ViewHolder {
        TextView deviceName;
        TextView deviceAddress;
        TextView deviceRSSI;
        TextView deviceUUID;
    }
}