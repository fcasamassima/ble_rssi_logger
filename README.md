# README #

Developing application to log RSSI values of Bluetooth Low Energy (BLE Beacons)



### What is this repository for? ###

The app is capable to log in a text file data coming from any BLE beacon, data stored are RSSI value and beacon advertising packet, together with beacon name

### How do I get set up? ###

To start is necessary to have Eclipse Android Develpment Tools (ADT), is possible to download the whole package here:

http://developer.android.com/sdk/index.html


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* filippo.casamassima@uniboit
